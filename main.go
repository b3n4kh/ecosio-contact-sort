package main

// "encoding/csv" won't demarshall into a struct that sexy
// Would have loved to use: https://github.com/gin-gonic/gin/blob/master/README.md#model-binding-and-validation but it doesn't support csv

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"sort"
	"time"

	"github.com/gocarina/gocsv"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// Contact of a Person with only first and lastname
type Contact struct {
	FirstName string `csv:"first_name" json:"first_name"`
	LastName  string `csv:"last_name" json:"last_name"`
}

func sortContacts(contacts []Contact) {
	sort.Slice(contacts, func(i, j int) bool {
		if contacts[i].LastName == contacts[j].LastName {
			return contacts[i].FirstName < contacts[j].FirstName
		}
		return contacts[i].LastName < contacts[j].LastName
	})
}

func parseCSVWithoutHeader(in io.Reader) []Contact {
	contacts := []Contact{}
	body, _ := ioutil.ReadAll(in)
	contactString := append(body, "first_name,last_name"...)
	err := gocsv.UnmarshalBytes(contactString, &contacts)
	if err != nil {
		fmt.Println(err)
	}
	return contacts
}

func parseCSV(in io.Reader) []Contact {
	contacts := []Contact{}
	err := gocsv.Unmarshal(in, &contacts)
	if err != nil {
		fmt.Println(err)
	}
	return contacts
}

func contactHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	if r.Header.Get("Content-type") != "text/csv" {
		http.Error(w, "Content-Type has to be text/csv", http.StatusUnsupportedMediaType)
		return
	}
	if r.Method != "POST" {
		http.Error(w, "Only POST allowed on this endpoint", http.StatusMethodNotAllowed)
		return
	}
	contacts := []Contact{}

	// If there are really no csv header which would be... messy
	// contacts = parseCSVWithoutHeader(r.Body)
	contacts = parseCSV(r.Body)
	sortContacts(contacts)

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(contacts)

	requestsProcessed.Inc()
	elapsed := float64(time.Since(start)) / float64(time.Second)
	requestHistogram.Observe(elapsed)
}

var (
	requestsProcessed = promauto.NewCounter(prometheus.CounterOpts{
		Name: "processed_requests",
		Help: "The total number of processed requests",
	})
	requestHistogram = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name: "processe_time",
		Help: "Histogram of the request durations", // Default Bucket is what we want
	})
)

func main() {
	mainServer := http.NewServeMux()
	mainServer.HandleFunc("/contact-sort", contactHandler)

	// I have little to no exp. with Prometheus so doing it by hand 4 fun.
	// Could have used https://github.com/zsais/go-gin-prometheus just took some hints there.
	promServer := http.NewServeMux()
	promServer.Handle("/", promhttp.Handler())

	fmt.Println("serving prometheus on  port 8000")
	go http.ListenAndServe(":8000", promServer)

	fmt.Println("serving contactsorter on  port 8080")

	http.ListenAndServe(":8080", mainServer)
}
