# Ecosio Contact Sort


Can be tested with:

`curl -X POST --data-binary '@data/test.csv' csort.b3n.eu/contact-sort  -H "Content-Type: text/csv"`



The Docker image should be built automatically with GitLab CI and automatically be pushed to the GitLab container registry (The free tier on gitlab.com includes 400 minutes CI for free per month). The programming language used to achieve the goal is up to you.

 

The resulting image should expose an HTTP port on 8080 which answers POST requests to “/contact-sort”. Please return appropriate response codes also for other URLs and methods.

 

The input data is sent as content-type text/csv with comma as a separator, the first name in the first column and the last name in the second column. The output is expected to be of type application/json. The output should be a JSON array with JSON objects consisting of the fields "last_name" and "first_name" (see samples below). The objects should be sorted alphabetically by last name. Entries with equal last name should be sorted by first name.

 

Sample input data:

```
first_name,last_name
Lisa,Nussbaum
Anne,Holzbaum
Peter,Burger
```
 

Sample output data:

```
[
  {"last_name": "Burger", "first_name": "Peter"},
  {"last_name": "Holzbaum", "first_name": "Anne"},
  {"last_name": "Nussbaum", "first_name": "Lisa"}
]
```
 

The docker container should automatically start the server on port 8080, without the need for any additional parameters.
 

The server should expose simple metrics in Prometheus format at port 8000. At least the following metrics should be exposed, more metrics can be added if you want:


Counter for the number of POST requests to “/contact-sort”.
Histogram of the request durations.
