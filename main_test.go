package main

import (
	"os"
	"reflect"
	"testing"
)

var testContacts = []Contact{
	Contact{"Lisa", "Nussbaum"},
	Contact{"Anne", "Holzbaum"},
	Contact{"Peter", "Burger"},
}

var sortedContacts = []Contact{
	Contact{"Peter", "Burger"},
	Contact{"Anne", "Holzbaum"},
	Contact{"Lisa", "Nussbaum"},
}

func TestParseCSV(t *testing.T) {
	testfile, err := os.Open("data/test.csv")
	if err != nil {
		t.Errorf("Could not read test.csv")
	}
	contacts := parseCSV(testfile)
	if !reflect.DeepEqual(contacts, testContacts) {
		t.Errorf("CSV not parsed correct")
	}
}

func TestSortContacts(t *testing.T) {
	sortContacts(testContacts)
	if !reflect.DeepEqual(testContacts, sortedContacts) {
		t.Errorf("Contacts not sorted correct")
	}
}
