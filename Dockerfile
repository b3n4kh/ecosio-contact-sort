FROM golang:alpine as build

WORKDIR /go/src/app

COPY . .

RUN go get -v -d ./...
RUN go build -o app

FROM alpine

COPY --from=build /go/src/app/app /usr/local/bin/app

EXPOSE 8080/tcp
EXPOSE 8000/tcp

ENTRYPOINT ["/usr/local/bin/app"]


