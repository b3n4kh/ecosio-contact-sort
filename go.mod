module gitlab.com/b3n4kh/ecosio-contact-sort

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/gocarina/gocsv v0.0.0-20200925213129-04be9ee2e1a2
	github.com/prometheus/client_golang v1.8.0
)
